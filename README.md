# Dota2Heroes

Android app that shows you heroes available in Dota 2. 

Using BE: https://api.opendota.com/api

Also I prepared some UI first in figma: https://www.figma.com/file/pDiAYIHGSUAQStHbicGveh/DOTAHEROES?type=design&node-id=0%3A1&mode=design&t=WfxFUSl8rS1Tua6w-1

Project follows udemy course: https://www.udemy.com/course/jetpack-compose-modern-app/ (original app using Marvel API but I decide to make my own project).

# Work in progress

For now it contains only splash screen.

## Features

App in full scale should contains possibility to show you all Dota 2 heroes, you can search by default values like in game by type (carry, support) or tags (nuker, disabler, durable) or attack type (range, meele) or attributes (strength, all) . You can also add your favourite heroes and also take notes about them. 

## Technology

Kotlin

Hilt

Room

Coil

Jetpack compose

MVVM architecture
